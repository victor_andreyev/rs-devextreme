import { Component } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import ODataStore from 'devextreme/data/odata/store';
import {ReportService } from './report.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent {

  dataSource: any;   
    fields: Array<any>;   
    filter: any;
    gridFilterValue: any;

    constructor(service: ReportService) {
        this.fields = service.getFields();
        this.filter = service.getFilter();
        this.gridFilterValue = this.filter;
        this.dataSource = new DataSource({
            store: new ODataStore({
                fieldTypes: {
                    "Product_Cost": "Decimal",
                    "Product_Sale_Price": "Decimal",
                    "Product_Retail_Price": "Decimal"
                },
                url: "https://js.devexpress.com/Demos/DevAV/odata/Products",
            }),              
            select: [
                'Product_ID',
                'Product_Name',
                'Product_Cost',
                'Product_Sale_Price',
                'Product_Retail_Price',
                'Product_Current_Inventory'
            ]
        });
    }
    buttonClick() {
        this.gridFilterValue = this.filter;
    }

}
