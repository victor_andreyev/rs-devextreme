import { Component} from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { ODataService } from './odata.service';

@Component({
  selector: 'app-odata',
  templateUrl: './odata.component.html',
  styleUrls: ['./odata.component.scss']
})
export class OdataComponent {

  dataSource: DataSource;
  collapsed = false;
  contentReady = (e) => {
      if(!this.collapsed) {
          this.collapsed = true;
          e.component.expandRow(["EnviroCare"]);
      }
  };
  customizeTooltip = (pointsInfo) => {
      return { text: parseInt(pointsInfo.originalValue) + "%" };
  }

  constructor(service: ODataService) {
      this.dataSource = service.getDataSource();
  }

}

