import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GridService } from './grid/grid.service';
import { ODataService } from './odata/odata.service';
import { ReportService } from './report/report.service';
import { FormService } from './form/form.service';
import { Appointment, CalendarService } from './calendar/calendar.service';
import { 
  DxTemplateModule,
  DxBulletModule,
  DxMenuModule,
  DxPopupModule,
  DxDataGridModule,
  DxSchedulerModule,
  DxSelectBoxModule,
  DxCheckBoxModule,
  DxFilterBuilderModule,
  DxButtonModule,
  DxNumberBoxModule,
  DxFormModule,
  DxAutocompleteModule
} from 'devextreme-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GridComponent } from './grid/grid.component';
import { CalendarComponent } from './calendar/calendar.component';
import { FormComponent } from './form/form.component';
import { ReportComponent } from './report/report.component';
import { PivotComponent } from './pivot/pivot.component';
import { OdataComponent } from './odata/odata.component';

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    CalendarComponent,
    FormComponent,
    ReportComponent,
    PivotComponent,
    OdataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DxMenuModule,
    DxPopupModule,
    DxDataGridModule,
    DxTemplateModule,
    DxBulletModule,
    DxSchedulerModule,
    DxSelectBoxModule,
    DxCheckBoxModule,
    DxFilterBuilderModule,
    DxButtonModule,
    DxNumberBoxModule,
    DxFormModule,
    DxAutocompleteModule
  ],
  providers: [ 
    GridService, 
    Appointment, 
    CalendarService, 
    ODataService, 
    ReportService,
    FormService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
