import { Component } from '@angular/core';
import {Appointment, CalendarService} from './calendar.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent {

  appointmentsData: Appointment[];
    currentDate: Date = new Date(2017, 4, 25);

    constructor(service: CalendarService) {
        this.appointmentsData = service.getAppointments();
    }

}
